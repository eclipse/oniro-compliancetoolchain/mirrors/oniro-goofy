# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT

# Define the slots and compatible strings for reference boards.
# The values here must be synchronized with those in the bundle.
# Look at meta-oniro-core/recipes-core/bundles/oniro-bundle-base.bb
RAUC_COMPAT:raspberrypi-armv7 := "Raspberry Pi ARMv7"
RAUC_SLOT_A:raspberrypi-armv7 := "/dev/mmcblk0p2"
RAUC_SLOT_B:raspberrypi-armv7 := "/dev/mmcblk0p3"

RAUC_COMPAT:raspberrypi4-64 := "Raspberry Pi 4"
RAUC_SLOT_A:raspberrypi4-64 := "/dev/mmcblk0p2"
RAUC_SLOT_B:raspberrypi4-64 := "/dev/mmcblk0p3"

RAUC_COMPAT:seco-intel-b68 := "SECO Alvin"
RAUC_SLOT_A:seco-intel-b68 := "/dev/sda2"
RAUC_SLOT_B:seco-intel-b68 := "/dev/sda3"

RAUC_COMPAT:seco-imx8mm-c61 := "SECO Astrid"
RAUC_SLOT_A:seco-imx8mm-c61 := "/dev/mmcblk0p2"
RAUC_SLOT_B:seco-imx8mm-c61 := "/dev/mmcblk0p3"

RAUC_COMPAT:seco-px30-d23 := "SECO Juno"
RAUC_SLOT_A:seco-px30-d23 := "/dev/mmcblk0p2"
RAUC_SLOT_B:seco-px30-d23 := "/dev/mmcblk0p3"

RAUC_COMPAT:qemux86 := "QEMU x86"
RAUC_SLOT_A:qemux86 := "/dev/sda2"
RAUC_SLOT_B:qemux86 := "/dev/sda3"

RAUC_COMPAT:qemux86-64 := "QEMU x86-64"
RAUC_SLOT_A:qemux86-64 := "/dev/sda2"
RAUC_SLOT_B:qemux86-64 := "/dev/sda3"

RAUC_COMPAT:qemuarm-efi:= "QEMU ARM (EFI)"
RAUC_SLOT_A:qemuarm-efi := "/dev/sda2"
RAUC_SLOT_B:qemuarm-efi := "/dev/sda3"

RAUC_COMPAT:qemuarm64-efi:= "QEMU ARM64 (EFI)"
RAUC_SLOT_A:qemuarm64-efi := "/dev/sda2"
RAUC_SLOT_B:qemuarm64-efi := "/dev/sda3"
